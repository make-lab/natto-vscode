![](./images/banner.png)

# Natto

> Supercharge your writing with AI: summarize, rewrite, translate, and elaborate any text.

**Natto** is a an extension for Visual Studio Code (VSCode) that provides users with the ability to highlight any section of text and easily manipulate it - fixing grammar, summarizing text, rewriting in different words, and translating it while adopting different _tones_.

Powering all these features is ChatGPT, which enables the extension to understand and manipulate text effectively.

![](./images/features.gif)

## Requirements

To utilize this extension, it's necessary to provide your own API key. This can be done by logging into OpenAI. If you don't already have an account, you'll need to create one. Once logged in, navigate to the [api-keys](https://platform.openai.com/api-keys) section and generate a new API key.

With the **Natto** extension installed, open VSCode. Proceed to the settings section, and look for **Natto-vscode: Open AI API Key**. This is where you'll paste the API key you generated earlier. Additionally, you have the option to select the engine you prefer to use, with 'gpt-3' being the default choice.

![](./images/settings.png)

## Release Notes

## [0.0.1]

- Initial public release

## [0.0.2]

- Added Sidebar with GUI
- Added translation
- Added selection of tone

## [0.0.3]

- New icon
- Added view container

## [0.0.4]

- Bug fixes
- New icons and README

## [0.0.6]

- Added support for `gpt-4o-mini`

## [0.0.8] and [0.0.9]

- Fixed against prompt injection
