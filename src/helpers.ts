import * as vscode from 'vscode';
import {
  elaborateWithGPT,
  fixtyposWithGPT,
  paraphraseWithGPT,
  summarizeWithGPT,
  traslateWithGPT,
} from './textgen';
import { EXTENSION_IDENTIFIER } from './extension';
import LANGUAGES from './languages';
import { SideViewProvider } from './panels/SidebarViewProvider';

type NattoSelection = {
  selection: string;
  range: vscode.Range;
};

const LANGUAGE_TONES = [
  'Default',
  'Academic',
  'Casual',
  'Formal',
  'Friendly',
  'Hilarious',
  'Humorous',
  'Professional',
  'Sarcastic',
  'Witty and clever',
];
type ValidTones = (typeof LANGUAGE_TONES)[number];

export async function fixGrammar() {
  try {
    const { selection, range } = getSelection();
    const replacement = await fixtyposWithGPT(selection);

    replaceSelection({
      selection: replacement,
      range: range,
    });

    promptForComparison(selection, 'Grammar fixed! 👨‍🔧');
  } catch (error: any) {
    vscode.window.showErrorMessage(error.message);
  }
}

export async function summarize() {
  try {
    const { selection, range } = getSelection();
    const replacement = await summarizeWithGPT(selection);

    replaceSelection({
      selection: replacement,
      range: range,
    });
    promptForComparison(selection, 'Summarized! 🔥');
  } catch (error: any) {
    vscode.window.showErrorMessage(error.message);
  }
}

export async function elaborate() {
  try {
    const { selection, range } = getSelection();
    const replacement = await elaborateWithGPT(selection);

    replaceSelection({
      selection: replacement,
      range: range,
    });
    promptForComparison(selection, 'Added more details! 🚀');
  } catch (error: any) {
    vscode.window.showErrorMessage(error.message);
  }
}

export async function paraphrase() {
  try {
    const { selection, range } = getSelection();
    const replacement = await paraphraseWithGPT(selection);

    replaceSelection({
      selection: replacement,
      range: range,
    });
    promptForComparison(selection, 'Rewritten in other words! 🤔');
  } catch (error: any) {
    vscode.window.showErrorMessage(error.message);
  }
}

export async function translate() {
  try {
    const { selection, range } = getSelection();
    const language = await vscode.window.showQuickPick(LANGUAGES, {
      title: 'Choose a language to translate to:',
      placeHolder: 'English',
      canPickMany: false,
    });
    if (!language) {
      return;
    }

    const replacement = await traslateWithGPT(selection, language);

    replaceSelection({
      selection: replacement,
      range: range,
    });
    promptForComparison(selection, 'Translated! 🤔');
  } catch (error: any) {
    vscode.window.showErrorMessage(error.message);
  }
}

export function selectModel() {
  const models = ['gpt-4o', 'gpt-4o-mini', 'gpt-4', 'gpt-3.5-turbo'];

  vscode.window.showQuickPick(models).then(async (model) => {
    if (model) {
      await vscode.workspace
        .getConfiguration(EXTENSION_IDENTIFIER)
        .update('openAI Engine', model, vscode.ConfigurationTarget.Global);
      vscode.window.showInformationMessage(`Selected model: ${model}`);
      updateSidebarView();
    }
  });
}

export function getModel(): string {
  return vscode.workspace
    .getConfiguration(EXTENSION_IDENTIFIER)
    .get('openAI Engine') as string;
}

export async function setLanguageTone() {
  const choice = await vscode.window.showQuickPick(LANGUAGE_TONES, {
    title: 'Pick a tone for your answers',
    placeHolder: 'Default tone',
    canPickMany: false,
  });
  if (choice) {
    await vscode.workspace
      .getConfiguration(EXTENSION_IDENTIFIER)
      .update('openAI Tone', choice, vscode.ConfigurationTarget.Global);
    vscode.window.showInformationMessage(`Selected tone: ${choice}`);
    updateSidebarView();
  }
}

export function getLanguageTone(): string {
  return vscode.workspace
    .getConfiguration(EXTENSION_IDENTIFIER)
    .get('openAI Tone') as string;
}

export function withProgress(callback: () => Promise<void>) {
  return () => {
    return vscode.window.withProgress(
      {
        location: vscode.ProgressLocation.Window,
        title: 'Working on it...',
        cancellable: false,
      },
      async (progress, token) => {
        token.onCancellationRequested(() => {
          console.log('User canceled the long running operation');
        });

        progress.report({ increment: 0 });
        await callback();
        progress.report({ increment: 100 });
      }
    );
  };
}

// Private

function getSelection(): NattoSelection {
  const editor = vscode.window.activeTextEditor;
  if (!editor) {
    throw new Error('No active text editor found.');
  }

  const selection = editor.selection;
  const text = editor.document.getText(selection);
  if (text.length === 0) {
    throw new Error('No text selected');
  }
  return { selection: text, range: selection };
}

function replaceSelection(newText: NattoSelection) {
  const editor = vscode.window.activeTextEditor;
  if (!editor) {
    return;
  }

  editor.edit((editBuilder) => {
    editBuilder.replace(newText.range, newText.selection);
  });
}

async function promptForComparison(original: string, message: string) {
  // save current clipboard
  const clipboard = await vscode.env.clipboard.readText();
  // copy text to clipboard
  vscode.env.clipboard.writeText(original);
  const pressed = await vscode.window.showInformationMessage(
    message,
    'See differences'
  );
  if (pressed) {
    vscode.commands.executeCommand(
      'workbench.files.action.compareWithClipboard'
    );
  }
  // restore clipboard
  vscode.env.clipboard.writeText(clipboard);
}

export function updateSidebarView() {
  const provider = SideViewProvider.getInstance();
  if (provider) {
    provider.sendMessage({ event: 'setModel', payload: getModel() });
    provider.sendMessage({ event: 'setTone', payload: getLanguageTone() });
  }
}
