/**
 * A wrapper for a SideView built with Svelte
 * The code for the sideview is src/ui/side_view
 */
import * as vscode from 'vscode';
import { getUri } from '../utilities/getUri';
import { getNonce } from '../utilities/getNonce';
import { updateSidebarView } from '../helpers';

// Internal type
type Message = {
  event: string;
  [payload: string]: string | boolean | object;
};

// The SideView provider that should be registered within the extension
class SideViewProvider implements vscode.WebviewViewProvider {
  public static readonly viewType = 'natto.view';

  private static _instance: SideViewProvider;
  private _extensionUri!: vscode.Uri;
  private _view?: vscode.WebviewView;

  // Singleton
  static getInstance() {
    if (!SideViewProvider._instance) {
      this._instance = new SideViewProvider();
    }
    return this._instance;
  }

  private constructor() {}

  public initialize(extensionUri: vscode.Uri) {
    this._extensionUri = extensionUri;
    return this;
  }

  public resolveWebviewView(
    webviewView: vscode.WebviewView,
    context: vscode.WebviewViewResolveContext,
    _token: vscode.CancellationToken
  ) {
    this._view = webviewView;

    webviewView.webview.options = {
      // Allow scripts in the webview
      enableScripts: true,
      localResourceRoots: [this._extensionUri],
    };

    webviewView.webview.html = this._getHtmlForWebview(webviewView.webview);

    // Incoming messages from the sideview
    webviewView.webview.onDidReceiveMessage((message: Message) => {
      switch (message.event) {
        case 'ready':
          updateSidebarView();
          break;
        case 'fixgrammarEvent':
          vscode.commands.executeCommand('natto-vscode.fixgrammar');
          break;
        case 'elaborateEvent':
          vscode.commands.executeCommand('natto-vscode.elaborate');
          break;
        case 'rewriteEvent':
          vscode.commands.executeCommand('natto-vscode.paraphrase');
          break;
        case 'summarizeEvent':
          vscode.commands.executeCommand('natto-vscode.summarize');
          break;
        case 'translateEvent':
          vscode.commands.executeCommand('natto-vscode.translate');
          break;
        case 'changeModelEvent':
          vscode.commands.executeCommand('natto-vscode.selectModel');
          break;
        case 'changeToneEvent':
          vscode.commands.executeCommand('natto-vscode.setLanguageTone');
          break;

        default:
          break;
      }
    });
  }

  public sendMessage(msg: Message) {
    if (this._view) {
      this._view.webview.postMessage(msg);
    }
  }

  private _getHtmlForWebview(webview: vscode.Webview) {
    // The CSS file from the Svelte build output
    const stylesUri = getUri(webview, this._extensionUri, [
      'webview-ui',
      'public',
      'build',
      'bundle.css',
    ]);
    // The JS file from the Svelte build output
    const scriptUri = getUri(webview, this._extensionUri, [
      'webview-ui',
      'public',
      'build',
      'bundle.js',
    ]);

    const nonce = getNonce();
    return `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <title>Hello World</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Security-Policy" content="default-src 'none'; style-src ${webview.cspSource}; script-src 'nonce-${nonce}';">
        <link rel="stylesheet" type="text/css" href="${stylesUri}">
        <script defer nonce="${nonce}" src="${scriptUri}"></script>
      </head>
      <body>
      </body>
    </html>`;
  }
}

export { SideViewProvider };
