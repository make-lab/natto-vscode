import * as vscode from 'vscode';
import {
  withProgress,
  fixGrammar,
  summarize,
  elaborate,
  paraphrase,
  translate,
  selectModel,
  setLanguageTone,
} from './helpers';
import { SideViewProvider } from './panels/SidebarViewProvider';

export const EXTENSION_IDENTIFIER = 'natto-vscode';

export async function activate(context: vscode.ExtensionContext) {
  // Side view
  context.subscriptions.push(
    vscode.window.registerWebviewViewProvider(
      SideViewProvider.viewType,
      SideViewProvider.getInstance().initialize(context.extensionUri)
    )
  );

  // Commands
  context.subscriptions.push(
    vscode.commands.registerCommand(
      'natto-vscode.fixgrammar',
      withProgress(fixGrammar)
    )
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      'natto-vscode.summarize',
      withProgress(summarize)
    )
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      'natto-vscode.paraphrase',
      withProgress(paraphrase)
    )
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      'natto-vscode.elaborate',
      withProgress(elaborate)
    )
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      'natto-vscode.translate',
      withProgress(translate)
    )
  );
  context.subscriptions.push(
    vscode.commands.registerCommand('natto-vscode.selectModel', selectModel)
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      'natto-vscode.setLanguageTone',
      setLanguageTone
    )
  );
}

export function deactivate() {}
